#-------------------------------------------------
#
# Project created by QtCreator 2020-05-06T14:35:47
#
#-------------------------------------------------

QT       += core gui

TARGET = OOP10
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    dialog.cpp \
    SmartPointer.cpp

HEADERS  += mainwindow.h \
    dialog.h \
    SmartPointer.h

FORMS    += mainwindow.ui \
    dialog.ui
