#ifndef SMARTPOINTER_H
#define SMARTPOINTER_H
template<typename T>
class SmartPointer //����� ���������
{
public:
   SmartPointer(T *ptr)
   {
       this->ptr =ptr;
   }

   ~SmartPointer()
   {
       delete ptr;
   }

   T& operator*()
   {
       return *ptr;
   }
   T* operator&()
   {
       return this->ptr;
   }
private:
    T *ptr;
};

template <typename T>
class SmartPointer<T[]> { // ������������� ������� ��� �������
    SmartPointer(T *ptr)
    {
        this->ptr =ptr;
    }

    ~SmartPointer()
    {
        delete[] ptr;
    }

    T& operator*()
    {
        return *ptr;
    }

    T* operator&()
    {
        return this->ptr;
    }

    T& operator[](int i)
    {
        return ptr[i];
    }
private:
    T* ptr;
};

#endif // SMARTPOINTER_H

